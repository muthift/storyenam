from selenium import webdriver
import unittest
class Story6FunctionalTest(unittest.TestCase):
    def setUp(self):
        chrome_options = Options()
        chrome_options.add_argument('--dns-prefetch-disable')
        chrome_options.add_argument('--no-sandbox')
        chrome_options.add_argument('--headless')
        chrome_options.add_argument('disable-gpu')
        service_log_path = "./chromedriver.log"
        service_args = ['--verbose']
        self.selenium  = webdriver.Chrome('./chromedriver', chrome_options=chrome_options)
        self.selenium.implicitly_wait(25) 
        super(Story6FunctionalTest,self).setUp()

    def tearDown(self):
        self.selenium.quit()
        super(Story6FunctionalTest, self).tearDown()

    def test_css(self):
        browser = webdriver.Chrome()
        browser.get('https://storyenammuthi.herokuapp.com/AppStory6/')
        assert 'Moethy' in browser.title