from django import forms
from .models import Todo
        
# class Todo_Form(forms.Form):
#     status = forms.CharField(label='', required=True, max_length=27)
class Todo_Form(forms.ModelForm):
    class Meta:
        model = Todo
        fields = ['status']
  