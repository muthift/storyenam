from django.shortcuts import render, reverse
from django.contrib.auth import authenticate, login
from django.http import HttpResponseRedirect, HttpResponse 
from .forms import Todo_Form 
from .models import Todo 
import urllib.request, json, requests

landing_page_content = 'Hello, Apa Kabar?'
def index(request):
    todo = Todo.objects.all()
    if request.method == 'POST':
        todo_form = Todo_Form(request.POST)
        if todo_form.is_valid():
            todo_form.save()
            return HttpResponseRedirect(reverse('index'))
    else:
        todo_form = Todo_Form()
    return render(request, 'landingpage.html', {'index': todo_form, 'yourStatus': todo, 'content': landing_page_content})


def profil(request):
    return render(request, 'Story41.html', {})

def favBook(request):
    return render(request, "favbook.html", {})

def dataJson(request):
    data = requests.get('https://www.googleapis.com/books/v1/volumes?q=quilting')
    return HttpResponse(data, content_type='application/json')

def ubahtema(request):
    return render(request, 'ubahtema1.html', {})

def home(request):
	return render(request, 'home.html')