from django.urls import path
from . import views

urlpatterns = [
    path('', views.index, name='index'),
    path('profil/', views.profil, name='profil'), 
    path('ubahtema/', views.ubahtema, name='ubahtema'),
    path('favBook/', views.favBook, name='favBook'),
    path('dataJson/', views.dataJson, name='dataJson'),
]
