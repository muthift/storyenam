from django.test import TestCase 
from django.test import Client 
from django.urls import resolve 
from django.http import HttpRequest
from .views import index, landing_page_content, profil
from .models import Todo 
from .forms import Todo_Form 
from selenium import webdriver
from selenium.webdriver.common.keys import Keys 
from selenium.webdriver.chrome.options import Options
import unittest 
import time

# class Story6FunctionalTest(unittest.TestCase):
#     def setUp(self):
#         chrome_options = Options()
#         chrome_options.add_argument('--dns-prefetch-disable')
#         chrome_options.add_argument('--no-sandbox')
#         chrome_options.add_argument('--headless')
#         chrome_options.add_argument('disable-gpu')
#         service_log_path = "./chromedriver.log"
#         service_args = ['--verbose']
#         self.selenium  = webdriver.Chrome('./chromedriver', chrome_options=chrome_options)
#         self.selenium.implicitly_wait(25) 
#         super(Story6FunctionalTest,self).setUp()

#     def tearDown(self):
#         self.selenium.quit()
#         super(Story6FunctionalTest, self).tearDown()

#     #functional tes
#     def test_functional_test(self):
#         self.selenium.get('https://storyenammuthi.herokuapp.com/AppStory6/')
#         cobaStatus = self.selenium.find_element_by_name('status')
#         cobaStatus.send_keys('Coba Coba')
#         cobaStatus.submit()
#         self.assertIn('Coba Coba', self.selenium.page_source)
#         time.sleep(3)
#         self.selenium.quit()

#     #cek css
#     def test_css(self):
#         self.selenium.get('https://storyenammuthi.herokuapp.com/AppStory6/')
#         cobaCSS = self.selenium.find_element_by_css_selector('p.status')
#         self.assertIn('Hello, Apa Kabar?', cobaCSS.text)
#         time.sleep(3)
    
#     #cek css
#     def test_css_2(self):
#         self.selenium.get('https://storyenammuthi.herokuapp.com/AppStory6/')
#         css=".logo"
#         self.assertNotIn('lala', css)
#         time.sleep(3)

#     # #cek positioning
#     def test_positioning(self):
#         self.selenium.get('https://storyenammuthi.herokuapp.com/AppStory6/')
#         tagP = self.selenium.find_element_by_tag_name('p').text
#         self.assertIn('Hello, Apa Kabar?', tagP)
#         time.sleep(3)

#     def test_positioning_2(self):
#         self.selenium.get('https://storyenammuthi.herokuapp.com/AppStory6/')
#         About = self.selenium.find_element_by_link_text('About').text
#         self.assertNotIn('Hello, Apa Kabar?', About)
#         time.sleep(3)

class Story6UnitTest(TestCase): 
    #cek url
    def test_story_6_url_is_exist(self): 
        response = Client().get('/AppStory6/') 
        self.assertEqual(response.status_code, 200)
    #cek url profil
    def test_story_6_profil_url_is_exist(self): 
        response = Client().get('/AppStory6/profil/') 
        self.assertEqual(response.status_code, 200) 
    #cek fungsinya
    def test_story_6_using_index_func(self): 
        found = resolve('/AppStory6/') 
        self.assertEqual(found.func, index) 
    #cek fungsi profile
    def test_story_6_profil_using_index_func(self): 
        found = resolve('/AppStory6/profil/') 
        self.assertEqual(found.func, profil) 
    #cek template
    def test_story_6_using_to_do_ist_template(self):
        response = Client().get('/AppStory6/')
        self.assertTemplateUsed(response, 'landingpage.html')
    #cek landing page
    def test_landing_page_is_completed(self):
        request = HttpRequest()
        response = index(request)
        html_response = response.content.decode('utf8')
        self.assertIn(landing_page_content, html_response)
    #cek model
    def test_model_can_create_new_todo(self):
        new_activity = Todo.objects.create(status='baik')
        counting_all_available_todo = Todo.objects.all().count()
        self.assertEqual(counting_all_available_todo, 1)
    #cek post database sukses ga moved permanently
    def test_lab5_post_success_and_render_the_result(self): 
        test = 'Hello' 
        response_post = Client().post('/AppStory6', {'content': test}) 
        self.assertEqual(response_post.status_code, 301) 
        response= Client().get('/AppStory6/') 
        html_response = response.content.decode('utf8') 
        self.assertIn(test, html_response) 
    #cek eror post database moved permanently
    def test_lab5_post_error_and_render_the_result(self): 
        test = 'Anonymous' 
        response_post = Client().post('/AppStory6', {'content': ''}) 
        self.assertEqual(response_post.status_code, 301) 
        response= Client().get('/AppStory6/') 
        html_response = response.content.decode('utf8') 
        self.assertNotIn(test, html_response)

if __name__ == '__main__':
    unittest.main(warnings='ignore')
