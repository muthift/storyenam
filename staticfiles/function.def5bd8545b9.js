// button when clicked
$(document).on('click','.buttonLove',function(){
    $('#counterButton').val(parseInt($('#counterButton').val()) + 1 );
    $(this).toggleClass("buttonLoveClicked");
});

// To make json to table in html
$(document).ready(function() {
  $.ajax({
    url: "/statusWeb/dataJson/",
    // dataType: "json",
    success: function(data) {
      var event_data = "";
      var dataBuku = data.items;
      for (var i = 0; i < dataBuku.length; i++) {
        event_data += '<tbody><tr id ="tableBook" class="ok"> ';
        event_data += "<td><img src='" + data.items[i].volumeInfo.imageLinks.thumbnail  + "'></td>";
        event_data += "<td>" + data.items[i].volumeInfo.title + "</td>";
        event_data += "<td>" + data.items[i].volumeInfo.publishedDate + "</td>";
        event_data += "<td>" + data.items[i].volumeInfo.publisher + "</td>";
        event_data +=
          '<td align ="center"><a href="#" class="btn btn-default buttonLove" title="add fav"><i class="glyphicon glyphicon-star" ></></a></td>';
        event_data += "</tr></tbody>";
      }
      $("#list_table_json").append(event_data);
    },
    error: function(d) {
      console.log("error");
      alert("404! Please wait until the File is Loaded.");
    }
  });
});
