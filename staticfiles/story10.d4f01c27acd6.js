$(document).on('click','.buttonLove',function(){
    $('#counter').val(parseInt($('#counter').val()) + 1 );
    $(this).toggleClass("buttonLoveClicked");
});

//story10
// ajax function for create user POST method

$.ajaxSetup({
    beforeSend: function(xhr) {
      xhr.setRequestHeader('Csrf-Token', '@helper.CSRF.getToken.value');
    }
});

function regSubscriber() {

    $.ajax({
        url: "/story10/",
        headers: {
            'Csrf-Token': '@play.filters.csrf.CSRF.getToken.map(_.value)'
        },
        type: "POST",
        data: {
            name      : $('#nameSubscriber').val(),
            email     : $('#emailSubscriber').val(),
            password  : $('#passwSubscriber').val()
        },

        success: function (json) {
            // console.log(json);
            $('#subscriber_regist').val(''); // empty form
            $('#msg_response').html("<div class='alert alert-success'><strong>Success!</strong> Account has been created.</div>")
        },

        error: function (xhr, errmsg, err) {
            $('#msg_response').html("<div class='alert alert-danger'><strong>Error!</strong>Something has wrong.</div>");
        },
    
    });
};


function validate_user() {
    //console.log("masuk validate user");
    $.ajax({
        url: "/story10/validate/",
        headers: {
            'Csrf-Token': '@play.filters.csrf.CSRF.getToken.map(_.value)'
        },
        type: "POST",
        data: {

            email     : $('#emailSubscriber').val(),
            name      : $('#nameSubscriber').val(),
            password  : $('#passwSubscriber').val()
            
        },

        success: function (response) {
            //console.log("masuk fungsi sukses");

            if (response.message == "All fields are valid"){

                document.getElementById('subscribe_button').disabled = false;
                $('#msg_validate').html("<p style='color:green'>"+ response.message + "</p>")

            }

            else {

                document.getElementById('subscribe_button').disabled = true;
                $('#msg_validate').html("<p style='color:red'>"+ response.message + "</p>")
            }
        },

        // debugging
        error: function (errmsg) {

            console.log(errmsg + "ERROR DETECTED");
        }

    });
};

$(document).ready(function () {
    var x_timer;
    $("#emailSubscriber").keyup(function (e) {
        //validate_user();
        clearTimeout(x_timer);
        var email = $(this).val();
        x_timer = setTimeout(function () {
            validate_user();
        }, 10);
    });
});

$(document).ready(function () {
    var x_timer;
    $("#nameSubscriber").keyup(function (e) {
        clearTimeout(x_timer);
        var password = $(this).val();
        x_timer = setTimeout(function () {
            validate_user();
        }, 10);

    });
});

$(document).ready(function () {
    var x_timer;
    $("#passwSubscriber").keyup(function (e) {
        clearTimeout(x_timer);
        var nama = $(this).val();
        x_timer = setTimeout(function () {
            validate_user();
        }, 10);
    });
});
