// buat button
function regSubscriber() {
    $.ajax({
        url: "/Story10/",
        type: "POST",
        data: {
            email     : $('#id_email').val(),
            name      : $('#id_name').val(),
            password  : $('#id_password').val()
        },

        success: function (json) {
            $('#msg_response').html("<div class='alert alert-success'><strong>Success!</strong> Account has been created.</div>")
        },

        error: function (xhr, errmsg, err) {
            $('#msg_response').html("<div class='alert alert-danger'><strong>Error!</strong>Something went wrong.</div>");
        },
    });
};

// buat validasi
function validate_user() {
    $.ajax({
        url: "/Story10/validate/",
        type: "POST",
        data: {
            email     : $('#id_email').val(),
            name      : $('#id_name').val(),
            password  : $('#id_password').val()
            
        },

        success: function (response) {
            // respons sama isi messsagenya kaya diviews
            if (response.message == "Success"){
                document.getElementById('id_button').disabled = false;
                $('#msg_validate').html("<p>"+ response.message + "</p>")
                // isi error atau berhasilnya
                // nambahin response message(sesuai sama yang ada di views) ke kelas msg_validate yang ada di html
            }

            else {
                document.getElementById('id_button').disabled = true;
                $('#msg_validate').html("<p>"+ response.message + "</p>")
            }
        },
        // debugging
        error: function (errmsg) {
            console.log("errornya:"+errmsg);
        }
    });
};

$(document).ready(function () {
    var x_timer;
    $("#id_email").keyup(function (e) {
        clearTimeout(x_timer);
        var email = $(this).val();
        x_timer = setTimeout(function () {
            validate_user();
        }, 10);
    });
});


$(document).ready(function () {
    var x_timer;
    $("#id_name").keyup(function (e) {
        clearTimeout(x_timer);
        var password = $(this).val();
        x_timer = setTimeout(function () {
            validate_user();
        }, 10);

    });
});

$(document).ready(function () {
    var x_timer;
    $("#id_password").keyup(function (e) {
        clearTimeout(x_timer);
        var nama = $(this).val();
        x_timer = setTimeout(function () {
            validate_user();
        }, 10);
    });
});
