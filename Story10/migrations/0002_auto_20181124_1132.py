# Generated by Django 2.1.1 on 2018-11-24 04:32

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('Story10', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='subscribe',
            name='email',
            field=models.EmailField(max_length=25, unique=True),
        ),
        migrations.AlterField(
            model_name='subscribe',
            name='name',
            field=models.CharField(max_length=25),
        ),
        migrations.AlterField(
            model_name='subscribe',
            name='password',
            field=models.CharField(max_length=25),
        ),
    ]
