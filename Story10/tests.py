from django.test import TestCase 
from django.test import Client 
from django.urls import resolve 
from django.http import HttpRequest
from .views import add_form, validate_form
from .models import Subscribe 
from .forms import Subscribe_Form 
import unittest 

class Story10UnitTest(TestCase): 
    #cek url
    def test_story_10_url_is_exist(self): 
        response = Client().get('/Story10/') 
        self.assertEqual(response.status_code, 200)
    #cek fungsinya
    def test_story_10_using_add_form_func(self): 
        found = resolve('/Story10/') 
        self.assertEqual(found.func, add_form) 
    #cek template
    def test_story_10_using_to_do_ist_template(self):
        response = Client().get('/Story10/')
        self.assertTemplateUsed(response, 'subscribe.html')

if __name__ == '__main__':
    unittest.main(warnings='ignore')
