from django.contrib import admin

from .models import Subscribe

class RegSubAdmin(admin.ModelAdmin):
    list_display = ['name', 'email', 'password']
admin.site.register(Subscribe, RegSubAdmin)