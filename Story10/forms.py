from django import forms
from .models import Subscribe
        
class Subscribe_Form(forms.ModelForm):
    class Meta:
        model = Subscribe
        fields = ['email', 'name', 'password']
        widgets = {
            'email': forms.TextInput(attrs={'type':'email','name':'email','class': 'form-control', 'id':'id_email','maxlength': 50}),
            'name': forms.TextInput(attrs={'type':'text','name':'name','class': 'form-control', 'id':'id_name','maxlength': 25}),
            'password': forms.TextInput(attrs={'type':'password','name':'password','class': 'form-control', 'id':'id_password','maxlength': 25}),
        }
