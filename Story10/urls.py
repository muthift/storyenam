from django.urls import path
from . import views

urlpatterns = [
    path('', views.add_form, name='add_form'),
    path('validate/', views.validate_form, name='validate_form'), 
    
]
