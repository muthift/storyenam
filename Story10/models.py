from django.db import models 

class Subscribe(models.Model): 
    email = models.EmailField(max_length=50, unique= True) 
    name = models.CharField(max_length=25)
    password = models.CharField(max_length=25)
