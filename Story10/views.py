from django.shortcuts import render, reverse
from django.http import HttpResponseRedirect, HttpResponse, JsonResponse 
from django.views.decorators.csrf import csrf_exempt
from .forms import Subscribe_Form 
from .models import Subscribe 
import urllib.request, json, requests

# 1. dari ajax if cek email di model return json
# 2. dari ajax if NamaForm(post.name, post email, post.password).is_valid() dan csrftoken valid MAKA save simpen ke model Response

# fungsi POST request subscriber. post method save user
@csrf_exempt   
def add_form(request):
    all_subs = Subscribe.objects.all()
    if request.method == 'POST':
        subscribe_form = Subscribe_Form(request.POST)
        if subscribe_form.is_valid():
            subscribe_form.save()
            return HttpResponseRedirect(reverse('add_form'))
    else:
        subscribe_form = Subscribe_Form()
    return render(request, 'subscribe.html', {'add_form': subscribe_form})

# fungsi untuk validasi email, name, password
@csrf_exempt    
def validate_form(request):
    if request.method == "POST":
        email = request.POST['email']
        name = request.POST['name']
        password = request.POST['password']

        is_taken = Subscribe.objects.filter(email=email)
        if len(is_taken) > 0 :
            return HttpResponse(json.dumps({"message": "Email tersebut sudah pernah didaftarkan sebelumnya, silakan daftar dengan email lain"}), content_type="application/json")
        if len(email) == 0 :
            return HttpResponse(json.dumps({"message": "Email can't be empty"}), content_type="application/json")  
        if len(name) == 0 :
            return HttpResponse(json.dumps({"message": "Name can't be empty"}), content_type="application/json")
        if len(password) == 0:
            return HttpResponse(json.dumps({"message": "Password can't be empty"}), content_type="application/json")
        
        return HttpResponse(json.dumps({"message": "Success"}), content_type="application/json")
  

        
        
    