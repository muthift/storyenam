from django.conf.urls import url, include
from django.contrib import admin
from django.contrib.auth import views
from AppStory6.views import home

urlpatterns = [
    url(r'^admin/', admin.site.urls),
    url(r'^login/$', views.LoginView.as_view(), name='LoginView'),
    url(r'^logout/$', views.LogoutView.as_view(), name='LogoutView'),
    url(r'^auth/', include('social_django.urls', namespace='social')),  
        url(r'^AppStory6/', include('AppStory6.urls')),#Nama app
        url(r'^Story10/', include('Story10.urls')),
]
